-- Combien d'étudiants ?
SELECT
    COUNT(*) AS nbrEtudiants
FROM
    test.etudiants;

-- ---------------------------------------------------------------------------------------------------------------------

-- Combien d'étudiants en 1TL1 ?
SELECT
    DISTINCT COUNT(e.Classe) AS 1TL1
FROM
    test.etudiants e
WHERE
    e.Classe = "1TL1";

-- ---------------------------------------------------------------------------------------------------------------------

-- Combien d'étudiants dans tous les "groupes" ?
SELECT
    e.Classe AS classe,
    COUNT(classe) AS nbr
FROM
    test.etudiants e
GROUP BY
    classe;

-- ---------------------------------------------------------------------------------------------------------------------

-- Combien d'étudiants par année-section ?
SELECT
    SUBSTR(e.Classe, 1, 2) AS AnSec,
    COUNT(e.Classe) AS nbr
FROM
    test.etudiants e
GROUP BY
    AnSec;

-- ---------------------------------------------------------------------------------------------------------------------

-- Combien d'étudiants par section ?
SELECT
    SUBSTR(e.Classe, 2,1) AS AnSec,
    COUNT(e.Classe) AS nbr
FROM
    test.etudiants e
GROUP BY
    AnSec;

-- ---------------------------------------------------------------------------------------------------------------------

-- Combien de fois reviennent chaque prénoms ?
SELECT
    e.Prenom AS prenom,
    COUNT(*) AS nbr
FROM
    test.etudiants e
GROUP BY
    prenom;

-- ---------------------------------------------------------------------------------------------------------------------

-- Quel est le plus grand nombre de fois que revient un prénom ?
SELECT
    MAX(countTable.samePrenomCount) AS nbr
FROM
    (
    SELECT
        e.Prenom AS prenom,
        COUNT(*) AS samePrenomCount
    FROM
        test.etudiants e
    GROUP BY
        prenom
    ) AS countTable;

-- ---------------------------------------------------------------------------------------------------------------------

-- Quel(s) est(sont) le(s) prénom(s) qui revien(nen)t le plus souvent ?
SELECT
    countTable.prenom AS prenom
FROM
    (
    SELECT
        e.Prenom AS prenom,
        COUNT(*) AS samePrenomCount
    FROM
        test.etudiants e
    GROUP BY
        prenom
    ORDER BY
        samePrenomCount DESC
    ) AS countTable;

-- ---------------------------------------------------------------------------------------------------------------------

-- Y-a-t-il un(des) cas d'homonymie ?
SELECT
    e.Nom AS nom,
    e.Prenom AS prenom,
    COUNT(*) AS nbr
FROM
    test.etudiants e
GROUP BY
    nom,
    prenom
HAVING
    nbr > 1;

-- ---------------------------------------------------------------------------------------------------------------------

-- Quels sont les étudiants concernés par une homonymie ?
SELECT
    e.Classe AS Classe,
    e.Matricule AS Matricule,
    e.Nom AS Nom,
    e.Prenom AS Prenom
FROM
    test.etudiants e,
    (
    SELECT
        e.Nom AS nom,
        e.Prenom AS prenom,
        COUNT(*) AS nbr
    FROM
        test.etudiants e
    GROUP BY
        nom,
        prenom
    HAVING
        nbr > 1
    ) AS countTable
WHERE
    e.Nom LIKE countTable.nom AND
    e.Prenom LIKE countTable.prenom;