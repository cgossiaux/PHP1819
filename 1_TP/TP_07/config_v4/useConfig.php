<?php

require_once 'Debug.inc.php';
require_once 'Config.inc.php';

$iDebug = new Debug(true);
$iConfig = new Config($iDebug);
$iConfig->load();

if ( isset($_POST['submit']) ) {
    if ( isset($_POST['submit']['configForm']) && $_POST['submit']['configForm'] == 'envoi') {
        $iConfig->save();
    }
}

echo $config = $iConfig->display();


