<?php

class Debug
{
    private $debug = false;
    private $msgList = [ ];


    public function __construct($param = false)
    {
        $this->setDebug($param);
    }

    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return string
     * http://jsonlogic.com/truthy.html
     */
    public function setDebug($debug = true)
    {
        $this->debug = $debug ? true : false ;
    }

     public function clear()
     {
        unset($msgList);
     }

     public function addMsg($msg)
     {
         //$msgList[] = $msg;
        // alternative
        array_push($this->msgList, $msg);
     }

     public function mPr($tab)
     {
         return '<pre>' . print_r($tab, true) . '</pre>';

     }

     public function send()
     {
        return $this->mPr($this->msgList);
     }

    public function debug()
    {

    }

}