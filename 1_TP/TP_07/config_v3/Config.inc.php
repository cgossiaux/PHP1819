<?php
require_once('Debug.inc.php');
class Config
{
    private $iDebug = null;
    private $file = 'config.ini.php';
    private $config = null;

    public function __construct($iDebug = null)
    {
        if($iDebug instanceof Debug) $this->iDebug = $iDebug;
        else $this->iDebug = new Debug();

    }


    public function setFile($file)
    {
        $this->file = $file;
    }

    function load($file = null) {
        if(!(is_null($file))) $this->setFile($file);
        $this->config = parse_ini_file('config.ini.php', true);
    }

    private function blocManager($blocName, $blocData, &$out ) {

        $oKey = ['min', 'max', 'pas'];

        foreach ($oKey as $aKey) {
            $$aKey = ( isset($blocData[$aKey]) ? $blocData[$aKey] : null );
            unset($blocData[$aKey]);

        }

        foreach ($blocData as $dataName => $dataValue) {
            $type = 'text';
            $id = "{$blocName}_$dataName";
            $name = "{$blocName}[$dataName]";
            $out[] = "<label for =\"$id\">$dataName</label>";

            switch ($dataName){
                case 'comment':
                    $out[] = "<textarea cols=\"50\" id=\"$id\" name=\"$name\" disabled required>";
                    $out[] = "$dataValue";
                    $out[] = "</textarea><br>";
                    break;
                case 'type':
                    $out[] = "<span id='$id' class='checkList'>";
                    $types = explode('|', $dataValue);
                    foreach ($types as $type) {
                        if ( isset($choix) ) {
                            if ( in_array($type, $choix) ) {
                                $options = 'checked';
                            } else {
                                $options = '';
                            }
                        }
                        $out[] = "<input type='checkbox' id='{$id}_$type' name='{$blocName}[choix][]' value='$type' $options>";
                        $out[] = "<label for='{$id}_$type'>$type</label>";
                    }
                    break;
                case 'taille':
                    $type = "number";
                    $options = ($min !== null ? "min=$min" : '') . ($max !== null ? "max=$max" : '') . ($pas !== null ? " step=$pas" : '');
                    if ($options) $options .= ' title="' . str_replace('step', 'pas', $options) . '"';

                default:
                    $out[] = "<input type=$type id=\"$id\" name=\"$name\" value=\"$dataValue\" $options required><br>";

            }
        }


    }

    public function display()
    {
        //return $this->iDebug->mPr($this->config);
        $out = [];
        $out[] = '<form action="#" method="post" id="nomConfig" name="idConfig">';

        foreach ($this->config as $key => $value) {
            $out[] = '<fieldset>';
            $out[] = '<legend>' . $key . '</legend>';
            $out[] = $this->blocManager($key, $value, $out) . '</fieldset>';
        }
        $out[] = '<input type="button" value="envoi">' . '</form>';
        return implode("\n", $out);
    }




}