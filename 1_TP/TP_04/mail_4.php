<?php

require_once 'INC/mesFonctions.inc.php';

$to = 'maxxduchene@gmail.com';
$from = $_POST['expéditeur'];
$subject = $_POST['sujet'];
$message = $_POST['message'];
$headers = "From: $from";

if(mail($to, $subject, $message, $headers)){
    echo 'Mail bien envoyé';

    $confirmTo = $_POST['expéditeur'];
    $confirmFrom = $to;
    $confirmSubject = 'Confirmation de votre prise de contact';
    $confirmHeaders = "From: $confirmFrom \r\n";
    $confirmHeaders .= "MIME-Version: 1.0 \r\n";
    $confirmHeaders .= 'Content-type: text/html;charset=UTF-8';
    $confirmMessage = "
        <table>
            <tr><th>Expéditeur</th><td>{$from}</td></tr>
            <tr><th>Sujet</th><td>{$subject}</td></tr>
            <tr><th>Contenu</th><td>{$message}</td></tr>
        </table>
    ";

    mail($confirmTo, $confirmSubject, $confirmMessage, $confirmHeaders);

} else{
    echo 'Echec de l\'envoi du mail';
}