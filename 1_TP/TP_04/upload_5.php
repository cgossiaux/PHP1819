<?php

require_once 'INC/mesFonctions.inc.php';

$file = $_FILES['file'];

echo monPrint_r($file);

if(is_uploaded_file($file['tmp_name'])){

    $acceptedFiles = ['image/jpeg', 'image/png', 'image/gif'];

    if(in_array($file['type'], $acceptedFiles)){
        $avatarMaxSize = 150;
        $uploadDir = 'AVATARS/';
        $imgName = $_POST['id'] . '.';
        $imgType = explode('/', $file['type'])[1];

        list($width_orig, $height_orig) = getimagesize($file['tmp_name']);

        $ratio_orig = $width_orig / $height_orig;

        if($ratio_orig < 1){
            $height = $avatarMaxSize * $ratio_orig;
            $width = $avatarMaxSize;
        } else{
            $height = $avatarMaxSize;
            $width = $avatarMaxSize * $ratio_orig;
        }

        $imageCreateFunction = 'imagecreatefrom' . $imgType;
        $imageFormatFunction = 'image' . $imgType;

        $resizedImg = imagecreatetruecolor($width, $height);
        $baseImg = $imageCreateFunction($file['tmp_name']);

        imagecopyresampled($resizedImg, $baseImg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        $imageFormatFunction($resizedImg, $uploadDir . $imgName . $imgType);

        echo '<a target="_blank" href="' . $uploadDir . $imgName . $imgType . '">Chemin vers le fichier</a>';
    } else{
        echo 'Files must be images';
    }

} else{
    echo 'no file to save';
}