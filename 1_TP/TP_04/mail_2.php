<?php

require_once 'INC/mesFonctions.inc.php';

$to = 'maxxduchene@gmail.com';
$subject = 'test';
$headers = "MIME-Version: 1.0 \r\n";
$headers .= 'Content-type:text/html;charset=UTF-8';

$protocol = scriptInfos('protocol');
$dns = scriptInfos('dns');
$firstDir = explode('/', scriptInfos('path'))[1];

$linkAdress = "{$protocol}://{$dns}/{$firstDir}/2_SITEX/phase_01/index.php";

$message = <<<MSG
<html>
<head>
    <title>HTML mail</title>
</head>
<body>
    <a href="{$linkAdress}">Lien vers le SITEX phase 01</a>
</body>
</html>
MSG;

ini_set('sendmail_from', 'he201372@students.ephec.be');
mail($to, $subject, $message, $headers);