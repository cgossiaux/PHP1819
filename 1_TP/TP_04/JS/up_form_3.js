function verif(){
    let acceptedExtensions = ["jpg", "jpeg", "gif", "png"];

    let submitNode = document.querySelector("input[type='submit'");
    let fileNode = document.querySelector("#file");
    let fileURL = fileNode.value;
    let extension = fileURL.split('.')[1];

    if(!document.querySelector("#fileInfo")){
        let infoNode = document.createElement('div');
        infoNode.setAttribute('id', 'fileInfo');
        fileNode.parentNode.insertBefore(infoNode, fileNode.nextSibling);
    }

    let infoNode = document.querySelector("#fileInfo");

    if(acceptedExtensions.indexOf(extension) === -1){
        submitNode.disabled = true;

        infoNode.style.color = "red";
        infoNode.textContent = 'Erreur. Extensions autorisees: jpg, jpeg, png, gif';

    } else{
        submitNode.disabled = false;

        let fileSize = fileNode.files[0].size;
        infoNode.style.color = "black";
        infoNode.textContent = "Size : " + String(fileSize) + " bytes.";
    }
}