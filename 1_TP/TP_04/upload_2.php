<?php

require_once 'INC/mesFonctions.inc.php';

$file = $_FILES['file'];
$acceptedFiles = ['jpg', 'jpeg', 'png', 'gif'];
$uploadDir = 'MES_IMAGES/';

echo monPrint_r($file);

if(is_uploaded_file($file['tmp_name'])){

    if(in_array(explode('/', $file['type'])[1], $acceptedFiles)){
        move_uploaded_file($file['tmp_name'], $uploadDir . $file['name']);
        echo '<a target="_blank" href="' . $uploadDir . $file['name'] . '">Chemin vers le fichier</a>';
    } else{
        echo 'Files must be images';
    }

} else{
    echo 'no file to save';
}