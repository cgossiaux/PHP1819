<?php
session_name('w3demo');
session_start();
?>
<!DOCTYPE html>
<html>
<body>
<?php
require_once '../INC/menu.inc.php';

$_SESSION['lastVisit']['get'] = date('Y D j G\hi\'s"');

// Echo session variables that were set on previous page
echo 'Session creation time : ' . $_SESSION['creationTime'] . '<br>';

foreach($_SESSION['lastVisit'] as $page => $date){
    echo 'Last visit on ' . $page . ' : ' . $date . '<br>';
}

echo "Favorite color is " . $_SESSION["favcolor"] . ".<br>";
echo "Favorite animal is " . $_SESSION["favanimal"] . ".";
echo ""

?>
</body>
</html>