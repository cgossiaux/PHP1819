<?php
session_name('w3demo');
session_start();
?>
<!DOCTYPE html>
<html>
<body>
<?php
require_once '../INC/menu.inc.php';

$_SESSION['lastVisit']['modify'] = date('Y D j G\hi\'s"');

// to change a session variable, just overwrite it
$_SESSION["favcolor"] = "yellow";
echo '<pre>' . print_r($_SESSION, true) . '</pre>';

?>
</body>
</html>