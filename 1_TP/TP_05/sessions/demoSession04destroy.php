<?php
session_name('w3demo');
session_start();
?>
<!DOCTYPE html>
<html>
<body>
<?php
require_once '../INC/menu.inc.php';

// remove all session variables
session_unset();

// destroy the session
session_destroy();

?>
</body>
</html>