<?php
session_name('w3demo');
session_start();
?>
<!DOCTYPE html>
<html>
<body>
<?php
require_once '../INC/menu.inc.php';

// Set session variables
if(!isset($_SESSION['creationTime'])) {
    $_SESSION['creationTime'] = date('Y D j G\hi\'s"');
}

$_SESSION['lastVisit']['start'] = date('Y D j G\hi\'s"');
$_SESSION["favcolor"] = "green";
$_SESSION["favanimal"] = "cat";

echo "Session variables are set.";

?>
</body>
</html>