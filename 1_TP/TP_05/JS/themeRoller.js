$('document').ready(function(){
    let reduireButton = document.createElement('input');
    reduireButton.setAttribute('type', 'button');
    reduireButton.setAttribute('id', 'reduireButton');
    reduireButton.setAttribute('value', 'Reduire');
    reduireButton.textContent = 'Reduire';
    reduireButton.style.position = 'absolute';
    reduireButton.style.top = '0px';
    reduireButton.style.left = '50%';

    let ouvrirButton = document.createElement('input');
    ouvrirButton.setAttribute('type', 'button');
    ouvrirButton.setAttribute('id', 'ouvrirButton');
    ouvrirButton.setAttribute('value', 'Ouvrir');
    ouvrirButton.textContent = 'Ouvrir';
    ouvrirButton.style.position = 'absolute';
    ouvrirButton.style.top = '0px';
    ouvrirButton.style.left = '50%';
    ouvrirButton.style.display = 'none';

    $('body').append(reduireButton, ouvrirButton);

    reduireButton.addEventListener('click', function(){
        reduire();
        reduireButton.style.display = 'none';
        ouvrirButton.style.display = 'block';
    });

    ouvrirButton.addEventListener('click', function(){
        ouvrir();
        ouvrirButton.style.display = 'none';
    })
});

function reduire(){
    $('div.themeroller__app-area').siblings().hide();
    $('footer').hide();
    $('h1').hide();
    $('nav').hide();
    $('div#logo-events.constrain.clearfix').hide();
    $('hr').hide();
    $('div#demo-options').hide();
    $('form#themeConfig').siblings().hide();
    $('#rollerTabsNav').hide();

    $('#container').css({
        'background-color': 'white',
        'border': 'none'
    });

    $('#content-wrapper').css({
        'border': 'none',
        'box-shadow' : 'none'
    });
}

function ouvrir(){
    $('.theme-group-content').show();
    $('div.themeroller__app-area, .application').css('width', '100%');
    $('.theme-group.clearfix').css('display', 'inline-block');
}