-- World

-- Afficher la superfie de chacune des regions d'europe
SELECT
    co.Region AS region,
    SUM(co.SurfaceArea) AS surface
FROM
    world.country co
WHERE
    co.Continent = "Europe"
        AND
    region LIKE "%Europe"
GROUP BY
    region
ORDER BY
    surface DESC;

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour chacun des continents, afficher le(s) pays qui a(ont) eu leur indépendence le plus récemment
SELECT
    co.Name AS name,
    co.Continent AS Continent,
    co.IndepYear AS IndepYear
FROM
    world.country AS co
        JOIN
    (SELECT
        co.Continent AS Continent,
        MAX(co.IndepYear) AS 'maxIndepYear'
    FROM
        world.country AS co
    GROUP BY
        Continent
    ) co_sub ON co.Continent = co_sub.Continent
        AND
    co.IndepYear = co_sub.maxIndepYear
ORDER BY
    co.Continent,
    co.Name;

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour le "monde", donnez le pourcentage de personnes qui ne parlent pas une langue officielle
SELECT
    SUM((coLa.Percentage/100) * co.population) /
    (SELECT
        SUM(co.population)
    FROM world.country AS co
    ) * 100 AS '%_langueNonOfficielle'
FROM
    world.countryLanguage AS coLa
        INNER JOIN
    world.country AS co ON co.code = coLa.CountryCode
WHERE
    coLa.isOfficial = 'F';

-- ---------------------------------------------------------------------------------------------------------------------

-- minicampus

-- Afficher la liste des facultés de base (celles qui n'ont pas de parent)
SELECT
    *
FROM
    minicampus.faculte fac
WHERE
    fac.codeParent IS NULL;

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher les filles d'une faculté donnée (p.e. TI)
SELECT
    *
FROM
    minicampus.faculte fac
WHERE
    fac.codeParent = "TI";

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour tous les étudiants, afficher les informations suivantes : groupe, matricule, nom, prénom et
-- email (construit : matricule@students...)
SELECT
    cl.nom AS groupe,
    u.username AS matricule,
    u.nom AS nom,
    u.prenom AS prenom,
    CONCAT(u.username, '@students.ephec.be') AS email
FROM
    minicampus.user u
        INNER JOIN
    minicampus.class_user clU ON u.id = clU.user_id
        INNER JOIN
    minicampus.class cl ON clU.class_id = cl.id
WHERE
    u.username LIKE 'HE%'
LIMIT
    3000;

-- ---------------------------------------------------------------------------------------------------------------------

-- pour chacune des facultés, afficher ces informations précédées du nom de son parent.
SELECT
    f2.nom,
    f1.id,
    f1.nom,
    f1.code,
    f1.codeParent,
    f1.position,
    f1.nbEnfants
FROM
    minicampus.faculte f1
        INNER JOIN
    minicampus.faculte f2 ON f1.codeParent = f2.code
WHERE
    f1.codeParent IS NOT NULL;