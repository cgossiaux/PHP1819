-- afficher la liste des bases de données disponibles
SHOW DATABASES;

-- ---------------------------------------------------------------------------------------------------------------------

-- afficher la liste des tables disponibles
SHOW TABLES;

-- ---------------------------------------------------------------------------------------------------------------------

-- afficher la liste des champs de la table etudiants
DESCRIBE etudiants;

-- ---------------------------------------------------------------------------------------------------------------------

-- World

-- Afficher la plus grande superficie pour un pays
SELECT
    MAX(c.SurfaceArea) AS laPlusGrande
FROM
    world.country c;

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher le pays qui a la plus petite superficie
SELECT
    c.Name,
    c.SurfaceArea AS "laPlusPetiteSuperf.(km²)"
FROM
    world.country c
WHERE
    c.SurfaceArea =
        (
        SELECT
            MIN(co.SurfaceArea)
        FROM
            world.country co
        );

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher la superfie totale du "BENELUX"
SELECT
    "benelux", SUM(c.SurfaceArea) AS surface
FROM
    world.country c
WHERE
    c.Name IN ("Belgium", "Netherlands", "Luxembourg");

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour les pays du "BENELUX", donnez : le nom du pays, la population du pays et la population totale des villes du pays
-- reprise dans la table des villes
SELECT
    co.Name AS pays,
    co.Population AS popuPays,
    SUM(ci.Population) AS popuVilles
FROM
    world.country co
        JOIN
    world.city ci ON co.Code = ci.CountryCode
WHERE
    co.Name IN ("Belgium", "Netherlands", "Luxembourg")
GROUP BY
    co.Name;

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour les pays du "BENELUX", donnez en ordre décroissant sur les pourcentages, les noms et les pourcentages des
-- langues parlées
SELECT
    cl.Language AS language,
    SUM(cl.Percentage/100*co.Population) /
        (SELECT
            SUM(co.Population)
        FROM
            world.country AS co
        WHERE
            co.Name IN ("Belgium", "Netherlands", "Luxembourg")
        ) * 100 AS pct
FROM
    world.country co
        JOIN
    world.countrylanguage cl ON co.Code = cl.CountryCode
WHERE
    co.Name IN ("Belgium", "Netherlands", "Luxembourg")
GROUP BY
    language
ORDER BY
    pct DESC;

-- ---------------------------------------------------------------------------------------------------------------------

-- Pour le "monde", donnez le pourcentage de personnes qui parlent une langue officielle
SELECT
    SUM((cl.Percentage/100)*co.Population) /
        (SELECT
            SUM(co.Population)
        FROM
            world.country AS co
        ) * 100 AS '%_langueOfficielle'
FROM
    world.countryLanguage AS cl
        INNER JOIN
    world.country AS co ON co.code = cl.CountryCode
WHERE
    cl.isOfficial = 'T';

-- ---------------------------------------------------------------------------------------------------------------------

-- Minicampus

-- Afficher la liste des classes de base (celles qui n'ont pas de parent)
SELECT
    *
FROM
    minicampus.class AS c
WHERE
    c.parent_id IS NULL;

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher les filles d'une section donnée (p.e. TI)

SELECT
    fac.nom AS nomFilles
FROM
    minicampus.faculte fac
WHERE
    fac.codeParent = "TI";

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher les filles des filles d'une classe donnée (p.e. TI)
SELECT
    fac.nom AS nomPetitesFilles
FROM
    minicampus.class fac
WHERE
    fac.parent_id BETWEEN 2 AND 4;

-- ---------------------------------------------------------------------------------------------------------------------

-- Afficher la liste des cours (code, faculté et libellé) pour une classe (p.e. 1TL2) donnée
SELECT
    co.code AS code,
    co.faculte AS faculte,
    co.intitule
FROM
    minicampus.cours co
        INNER JOIN
    minicampus.course_class coCl ON co.code = coCl.cours_id
        INNER JOIN
    minicampus.class cl ON coCl.class_id = cl.id
WHERE
    cl.nom = "1TL2";