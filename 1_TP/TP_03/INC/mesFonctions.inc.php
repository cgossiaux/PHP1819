<?php

function scriptInfos($param = "new"){
    static $out;

    $param = strtolower($param);

    $isHTTPS = isset($_SERVER['HTTPS']);
    $pathInfo = pathinfo($_SERVER['URL']);

    switch ($param){
        case 'new' :
            $out = array(
                'protocol' => $isHTTPS ? 'https' : 'http',
                'port' => $_SERVER['SERVER_PORT'],
                'isportdef' => $_SERVER['SERVER_PORT_SECURE'],
                'dns' => $_SERVER['SERVER_NAME'],
                'path' => $pathInfo['dirname'],
                'name' => $pathInfo['basename'],
                'short' => $pathInfo['filename'],
                'ext' => $pathInfo['extension']
            );

            return $out;

        case "empty" :
            $out = null;
            break;

        case "all" :
            if(is_null($out)) $out = scriptInfos();

            return $out;

        case "protocol" :
            if(is_null($out)) $out = scriptInfos();

            return $out['protocol'];

        case "port" :
            if(is_null($out)) $out = scriptInfos();

            return $out['port'];

        case "isportdef" :
            if(is_null($out)) $out = scriptInfos();

            return $out['isportdef'];

        case "dns" :
            if(is_null($out)) $out = scriptInfos();

            return $out['dns'];

        case "path" :
            if(is_null($out)) $out = scriptInfos();

            return $out['path'];

        case "name" :
            if(is_null($out)) $out = scriptInfos();

            return $out['name'];

        case "short" :
            if(is_null($out)) $out = scriptInfos();

            return $out['short'];

        case "ext" :
            if(is_null($out)) $out = scriptInfos();

            return $out['ext'];

        case "full" :
            if(is_null($out)) $out = scriptInfos();

            $str = $out['protocol'] . '://' . $out['dns'] . ($out['isportdef'] ? '' : ':' . $out['port'])
                   . $out['path'] . $out['name'];

            return $str;

        case "root" :
            if(is_null($out)) $out = scriptInfos();

            return $out['path'] . $out['name'];

        default :
            return 'Error in ' . __FUNCTION__ . ' paramètre inconnu (' . $param . ')';

    }
}


function creeTableau($uneListe, $titre = '', $index = false){
    $out ='';

    $out .= '<table>';
    $out .= '<caption>' . $titre . '</caption>';
    $out .= '<tr>';

    if ($index) $out .= '<th>index</th>';

    if(!empty($uneListe)){

        $keys = array_keys($uneListe[array_keys($uneListe)[0]]);

        foreach($keys as $key) $out .= '<th>' . $key . '</th>';

        $out .= '</tr>';

        foreach ($uneListe as $key => $value) {
            $out .= '<tr>';

            if ($index) $out .= '<td>' . $key . '</td>';
            foreach ($uneListe[$key] as $innerKey => $innerValue) {
                $out .= '<td>' . $innerValue . '</td>';
            }
            $out .= '</tr>';
        }
    } else{
        $out .= '<th>valeur</th>';
        $out .= '</tr>';
    }

    $out .= '</table>';

    return $out;
}


function monPrint_r($tab){
    return '<pre>' . print_r($tab, true) . '</pre>';
}

function getServer(){
    $out = '';

    $possibleDNS = array('devweb.ephec.be', '193.190.65.93');

    if(in_array(scriptInfos('dns'), $possibleDNS)){
        $out .= 'localhost';
    } else{
        $out .= $possibleDNS[0];
    }

    return $out;
}