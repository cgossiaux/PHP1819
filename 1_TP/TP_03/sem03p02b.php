<?php

require_once 'INC/dbConnect.inc.php';
require_once 'INC/mesFonctions.inc.php';

$dbHost = getServer();
$dbName = 'minicampus';
$groupe = isset($_GET['groupe']) ? $_GET['groupe'] : '1TL1';

$sql = <<<SQL
SELECT
    co.code AS code,
    co.faculte AS faculte,
    co.intitule
FROM
    minicampus.cours co
        INNER JOIN
    minicampus.course_class coCl ON co.code = coCl.cours_id
        INNER JOIN
    minicampus.class cl ON coCl.class_id = cl.id
WHERE
    cl.nom = ?
ORDER BY
    code
;
SQL;

try{

    /** @var array $__INFOS__ */
    $dbh = new PDO("mysql:host={$dbHost}; dbname={$dbName}", $__INFOS__['user'], $__INFOS__['pswd']);

    $sth = $dbh->prepare($sql);
    $sth->execute(array($groupe));
    $res = $sth->fetchAll(PDO::FETCH_ASSOC);

    echo creeTableau($res, 'AVEC index', true);

    $dbh = null;
} catch(PDOException $e){
    print 'Error ! : ' . $e -> getMessage() . '<br>';
    die();
}
