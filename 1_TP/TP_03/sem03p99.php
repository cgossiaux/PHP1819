<?php
$groupe = !empty($_GET['groupe']) ? $_GET['groupe'] : '';
?>
<form action="" method="get">
    <input type="text" placeholder="groupe recherché" name="groupe" value="<?= $groupe ?>">
    <input type="submit" value="envoi">
</form>
<?php

if( !isset($_GET['groupe']) || $_GET['groupe'] === '') die();

require_once 'INC/dbConnect.inc.php';
require_once 'INC/mesFonctions.inc.php';

try{
    $_oh__oh___oh = '';
    $dbHost = getServer();

    /** @var array $__INFOS__ */
    $dbh = new PDO("mysql:host={$dbHost}; dbname={$__INFOS__['dbName']}", $__INFOS__['user'], $__INFOS__['pswd']);

    $sql = <<<SQL
        call mc_groupWithParent(?);
SQL;

    $sth = $dbh->prepare($sql);
    $sth->execute(array($groupe));
    $groupeInfos = $sth->fetchAll(PDO::FETCH_ASSOC);

    if($groupeInfos){
        if(count($groupeInfos) > 1){
            $_oh__oh___oh .= creeTableau(array_map(function($value){
                return ['valeur' => $value['nomParent'] . ' - ' . $value['nom']];
            }, $groupeInfos), 'Plusieurs groupes de même nom V2');
        }
        else{
            $_oh__oh___oh .= 'Groupe : ' . $groupe . '<br>';
            $_oh__oh___oh .= 'Nom du parent : ' . $groupeInfos[0]['nomParent'] . '<br>';$sql = <<<SQL
                call mc_coursesGroup(?)
SQL;

            $sth = $dbh->prepare($sql);
            $sth ->execute(array($groupe));
            $res = $sth->fetchAll(PDO::FETCH_ASSOC);

            if($res) $_oh__oh___oh .= creeTableau($res, 'AVEC index', true);
            else $_oh__oh___oh .= 'Aucun cours n\'est rattaché à ce groupe !';
        }
    } else{
        $_oh__oh___oh .= 'Ce groupe n\'existe pas';
    }

    $dbh = null;
    echo $_oh__oh___oh;

} catch(PDOException $e){
    print 'Error ! : ' . $e -> getMessage() . '<br>';
    die();
}