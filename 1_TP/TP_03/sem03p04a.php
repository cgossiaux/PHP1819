<?php
$groupe = !empty($_GET['groupe']) ? $_GET['groupe'] : '';
?>
<form action="" method="get">
    <input type="text" placeholder="groupe recherché" name="groupe" value="<?= $groupe ?>">
    <input type="submit" value="envoi">
</form>
<?php

if( !isset($_GET['groupe']) || $_GET['groupe'] === '') die();

require_once 'INC/dbConnect.inc.php';
require_once 'INC/mesFonctions.inc.php';

try{
    $_oh__oh___oh = '';
    $dbHost = getServer();
    $dbName = 'minicampus';

    $sql = <<<SQL
        SELECT
            cl1.id,
            cl2.nom as parentName
        FROM
            minicampus.class cl1
                INNER JOIN
            minicampus.class cl2 ON cl1.parent_id = cl2.id
        WHERE
            cl1.nom = ? ;
SQL;

    /** @var array $__INFOS__ */
    $dbh = new PDO("mysql:host={$dbHost}; dbname={$dbName}", $__INFOS__['user'], $__INFOS__['pswd']);

    $sth = $dbh->prepare($sql);
    $sth->execute(array($groupe));
    $groupeInfos = $sth->fetchAll(PDO::FETCH_ASSOC);

    if($groupeInfos){
        $sql = <<<SQL
            SELECT
                co.code AS code,
                co.faculte AS faculte,
                co.intitule
            FROM
                minicampus.cours co
                    INNER JOIN
                minicampus.course_class coCl ON co.code = coCl.cours_id
                    INNER JOIN
                minicampus.class cl ON coCl.class_id = cl.id
            WHERE
                cl.nom = ?
            ORDER BY
                code;
SQL;

        $sth = $dbh->prepare($sql);
        $sth ->execute(array($groupe));
        $res = $sth->fetchAll(PDO::FETCH_ASSOC);

        $_oh__oh___oh .= 'Groupe : ' . $groupe . '<br>';
        $_oh__oh___oh .= 'Nom du parent : ' . $groupeInfos[0]['parentName'] . '<br>';

        if($res){
            $_oh__oh___oh .= creeTableau($res, 'AVEC index', true);
        } else{
            $_oh__oh___oh .= 'Aucun cours n\'est rattaché à ce groupe !';
        }

        $dbh = null;
    } else{
        $_oh__oh___oh .= 'Ce groupe n\'exixte pas';
        $dbh = null;
    }

    echo $_oh__oh___oh;

} catch(PDOException $e){
    print 'Error ! : ' . $e -> getMessage() . '<br>';
    die();
}
