<?php

require_once 'INC/dbConnect.inc.php';
require_once 'INC/mesFonctions.inc.php';

$dbHost = getServer();
$dbName = 'minicampus';
$sql = <<<'EOD'
SELECT
    co.code AS code,
    co.faculte AS faculte,
    co.intitule
FROM
    minicampus.cours co
        INNER JOIN
    minicampus.course_class coCl ON co.code = coCl.cours_id
        INNER JOIN
    minicampus.class cl ON coCl.class_id = cl.id
WHERE
    cl.nom = "1TL2"
ORDER BY
    code
;
EOD;

try{

    /** @var array $__INFOS__ */
    $dbh = new PDO("mysql:host={$dbHost}; dbname={$dbName}", $__INFOS__['user'], $__INFOS__['pswd']);

    echo creeTableau($dbh->query($sql, PDO::FETCH_ASSOC)->fetchAll(), 'avec index', true);

    $dbh = null;
} catch(PDOException $e){
    print 'Error ! : ' . $e -> getMessage() . '<br>';
    die();
}
