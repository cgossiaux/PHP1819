-- MySQL dump 10.17  Distrib 10.3.14-MariaDB, for Linux (x86_64)
--
-- Host: devweb.ephec.be    Database: 1819he201265
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbprofil`
--

DROP TABLE IF EXISTS `tbprofil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbprofil` (
  `pId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pNom` char(20) NOT NULL,
  `pAbrev` char(10) NOT NULL,
  `pIcon` blob,
  `pEstStatus` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`pId`),
  UNIQUE KEY `pNom_UNIQUE` (`pNom`),
  UNIQUE KEY `pAbrev_UNIQUE` (`pAbrev`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbprofil`
--

LOCK TABLES `tbprofil` WRITE;
/*!40000 ALTER TABLE `tbprofil` DISABLE KEYS */;
INSERT INTO `tbprofil` VALUES (1,'anonyme','ano',NULL,NULL),(2,'activation','acti',NULL,1),(3,'membre','memb',NULL,NULL),(4,'réactivation','réact',NULL,1),(5,'mdp-perdu','mdpp',NULL,1),(6,'modérateur','modo',NULL,NULL),(7,'sous-administrateur','sAdmin',NULL,NULL),(8,'administrateur','admin',NULL,NULL);
/*!40000 ALTER TABLE `tbprofil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbuser`
--

DROP TABLE IF EXISTS `tbuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbuser` (
  `uId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uPseudo` char(20) NOT NULL,
  `uEmail` char(50) NOT NULL,
  `uSemence` char(32) NOT NULL,
  `uMdp` char(32) NOT NULL,
  `uQuestion` varchar(100) DEFAULT 'null',
  `uReponse` varchar(50) DEFAULT 'null',
  `uAvatar` blob,
  `uDateCreation` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uId`,`uPseudo`),
  UNIQUE KEY `uPseudo_UNIQUE` (`uPseudo`),
  UNIQUE KEY `uId_UNIQUE` (`uId`),
  UNIQUE KEY `uEmail_UNIQUE` (`uEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbuser`
--

LOCK TABLES `tbuser` WRITE;
/*!40000 ALTER TABLE `tbuser` DISABLE KEYS */;
INSERT INTO `tbuser` VALUES (1,'ano','ano@ici.be','394944d695e142d30d9ea36c4b345676','5e531d0e30e661d3a5bd0f1de6e17f50',NULL,NULL,NULL,NULL),(2,'acti','acti@ici.be','394944d695e142d30d9ea36c4b345676','f02e24de34b8982f6255438d34a6800b',NULL,NULL,NULL,NULL),(3,'memb','memb@ici.be','394944d695e142d30d9ea36c4b345676','98183ba62c72254a3a1c97cce9945fc3',NULL,NULL,NULL,NULL),(4,'réact','réact@ici.be','394944d695e142d30d9ea36c4b345676','b946086c7e9a6f661e8e024ffaed8892',NULL,NULL,NULL,NULL),(5,'mdpp','mdpp@ici.be','394944d695e142d30d9ea36c4b345676','f211e7e63756f2569e18c01aa47fbd55',NULL,NULL,NULL,NULL),(6,'modo','modo@ici.be','394944d695e142d30d9ea36c4b345676','ee94a53b0a83bb23c511ddfa1df6c68b',NULL,NULL,NULL,NULL),(7,'sAdmin','sAdmin@ici.be','394944d695e142d30d9ea36c4b345676','9a2a9ab7b57053f8a11b99028c2497ca',NULL,NULL,NULL,NULL),(8,'admin','admin@ici.be','394944d695e142d30d9ea36c4b345676','bd244e9fb7bd00f7ccec047dd49dc20f',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbuserprofil`
--

DROP TABLE IF EXISTS `tbuserprofil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbuserprofil` (
  `pId` int(10) unsigned NOT NULL,
  `uId` int(10) unsigned NOT NULL,
  `upDataDebut` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uId`,`pId`),
  KEY `fk_tbUserProfil_tbProfil1_idx` (`pId`),
  CONSTRAINT `fk_tbUserProfil_tbProfil1` FOREIGN KEY (`pId`) REFERENCES `tbprofil` (`pId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_tbUserProfil_tbUser` FOREIGN KEY (`uId`) REFERENCES `tbuser` (`uId`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbuserprofil`
--

LOCK TABLES `tbuserprofil` WRITE;
/*!40000 ALTER TABLE `tbuserprofil` DISABLE KEYS */;
INSERT INTO `tbuserprofil` VALUES (1,1,'2019-05-16 00:48:03'),(2,2,'2019-05-16 00:48:03'),(2,3,'2019-05-16 00:48:03'),(3,3,'2019-05-16 00:48:03'),(4,3,'2019-05-16 00:48:03'),(5,3,'2019-05-16 00:48:03'),(6,3,'2019-05-16 00:48:03'),(7,3,'2019-05-16 00:48:03'),(8,3,'2019-05-16 00:48:04'),(4,4,'2019-05-16 00:48:03'),(5,5,'2019-05-16 00:48:03'),(6,6,'2019-05-16 00:48:03'),(7,6,'2019-05-16 00:48:03'),(8,6,'2019-05-16 00:48:04'),(7,7,'2019-05-16 00:48:04'),(8,7,'2019-05-16 00:48:04'),(8,8,'2019-05-16 00:48:04');
/*!40000 ALTER TABLE `tbuserprofil` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16  1:02:46
