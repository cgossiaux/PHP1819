-- -----------------------------------------------------
-- Data for table `tbUser`
-- -----------------------------------------------------
START TRANSACTION;
USE `1819he201265`;
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (1, 'ano', 'ano@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'anonyme')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (2, 'acti', 'acti@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'activation')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (3, 'memb', 'memb@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'membre')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (4, 'réact', 'réact@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'réactivation')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (5, 'mdpp', 'mdpp@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'mdp-perdu')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (6, 'modo', 'modo@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'modérateur')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (7, 'sAdmin', 'sAdmin@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'sous-administrateur')), NULL, NULL, NULL, NULL);
INSERT INTO `tbUser` (`uId`, `uPseudo`, `uEmail`, `uSemence`, `uMdp`, `uQuestion`, `uReponse`, `uAvatar`, `uDateCreation`) VALUES (8, 'admin', 'admin@ici.be', MD5(UNIX_TIMESTAMP()), MD5(CONCAT(MD5(UNIX_TIMESTAMP()),'administrateur')), NULL, NULL, NULL, NULL);


COMMIT;


-- -----------------------------------------------------
-- Data for table `tbProfil`
-- -----------------------------------------------------
START TRANSACTION;
USE `1819he201265`;
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'anonyme', 'ano', NULL, NULL);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'activation', 'acti', NULL, 1);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'membre', 'memb', NULL, NULL);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'réactivation', 'réact', NULL, 1);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'mdp-perdu', 'mdpp', NULL, 1);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'modérateur', 'modo', NULL, NULL);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'sous-administrateur', 'sAdmin', NULL, NULL);
INSERT INTO `tbProfil` (`pId`, `pNom`, `pAbrev`, `pIcon`, `pEstStatus`) VALUES (DEFAULT, 'administrateur', 'admin', NULL, NULL);


COMMIT;


-- -----------------------------------------------------
-- Data for table `tbUserProfil`
-- -----------------------------------------------------
START TRANSACTION;
USE `1819he201265`;
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (1, 1, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (2, 2, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (2, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (3, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (4, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (4, 4, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (5, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (5, 5, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (6, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (6, 6, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (7, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (7, 6, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (7, 7, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (8, 3, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (8, 6, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (8, 7, NOW());
INSERT INTO `tbUserProfil` (`pId`, `uId`, `upDataDebut`) VALUES (8, 8, NOW());

COMMIT;