-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema 1819he201265
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `1819he201265` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `1819he201265` ;

-- -----------------------------------------------------
-- Table `tbUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbUser` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbUser` (
  `uId` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `uPseudo` CHAR(20) NOT NULL,
  `uEmail` CHAR(50) NOT NULL,
  `uSemence` CHAR(32) NOT NULL,
  `uMdp` CHAR(32) NOT NULL,
  `uQuestion` VARCHAR(100) NULL DEFAULT 'null',
  `uReponse` VARCHAR(50) NULL DEFAULT 'null',
  `uAvatar` BLOB NULL,
  `uDateCreation` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uId`,`uPseudo`),
  UNIQUE KEY `uPseudo_UNIQUE` (`uPseudo`),
  UNIQUE KEY `uId_UNIQUE` (`uId`),
  UNIQUE KEY `uEmail_UNIQUE` (`uEmail`)
);

-- -----------------------------------------------------
-- Table `tbProfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbProfil` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbProfil` (
  `pId` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pNom` CHAR(20) NOT NULL,
  `pAbrev` CHAR(10) NOT NULL,
  `pIcon` BLOB NULL,
  `pEstStatus` TINYINT(4) NULL DEFAULT 1,
  PRIMARY KEY (`pId`),
  UNIQUE KEY `pNom_UNIQUE` (`pNom`),
  UNIQUE KEY `pAbrev_UNIQUE` (`pAbrev`)
);

-- -----------------------------------------------------
-- Table `tbUserProfil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbUserProfil` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tbUserProfil` (
  `pId` INT UNSIGNED NOT NULL,
  `uId` INT UNSIGNED NOT NULL,
  `upDataDebut` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`uId`,`pId`),
  KEY `fk_tbUserProfil_tbProfil1_idx` (`pId`),
  CONSTRAINT `fk_tbUserProfil_tbProfil1`
    FOREIGN KEY (`pId`)
    REFERENCES `tbProfil` (`pId`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbUserProfil_tbUser`
    FOREIGN KEY (`uId`)
    REFERENCES `tbUser` (`uId`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
  );
