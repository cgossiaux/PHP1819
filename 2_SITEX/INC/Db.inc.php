<?php

class Db
{
    private $_db           = [];
    private $_iDebug       = null;
    private $_iPdo         = null;
    private $_pdoException = null;

    public function __construct($iDebug)
    {
        if ($iDebug instanceof Debug) $this->_iDebug = $iDebug;
        else $this->_iDebug = new Debug();

        $this->getDbInfos();
        $this->connect();
    }

    private function getDbInfos()
    {
        //$this->_db =& $GLOBALS['infos']['db'];

        $this->_db = array_combine(
            ['h', 'u', 'p', 'n'],
            $_SESSION['cfg']['DB']
        );

        $this->_db['h'] = $this->getCorrectServer();
    }

    private function getCorrectServer()
    {
        $possibleDNS = array('134.209.205.81');
        if(in_array($_SERVER['SERVER_NAME'], $possibleDNS)) return 'localhost';
        else return $possibleDNS[0];
    }

    private function connect()
    {
        try{
            $host   = $this->_db['h'];
            $user   = $this->_db['u'];
            $pswd   = $this->_db['p'];
            $dbName = $this->_db['n'];
            $dsn    = "mysql:host=$host;dbname=$dbName";

            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ];

            $this->_iPdo = new PDO($dsn, $user, $pswd, $options);
            $this->_iDebug->addMsg('Connexion DB OK');
        } catch (PDOException $e) {
            $this->_pdoException = $e->getMessage();
            $this->_iDebug->addMsg('PDOException : ' . $this->_pdoException);
        }
    }

    private function clearException()
    {
        $this->_pdoException = null;
    }

    public function getException()
    {
        return $this->_pdoException;
    }

    public function testCall()
    {
        try{
            $sth = $this->_iPdo->prepare('call mc_allGroups()');
            $sth->execute();
            return $sth->fetchAll();
        } catch (PDOException $e) {
            $this->_pdoException = $e->getMessage();
            $this->_iDebug->addMsg('PDOException : ' . $this->_pdoException);
        }
    }

    public function testCall_1P($param)
    {
        try{
            $sth = $this->_iPdo->prepare('call mc_coursesGroup(?)');
            $sth->execute( array($param) );
            return $sth->fetchAll();
        } catch (PDOException $e) {
            $this->_pdoException = $e->getMessage();
            $this->_iDebug->addMsg('PDOException : ' . $this->_pdoException);
        }
    }

    public function call($procName, $paramArray = [])
    {
        $nP = [];

        switch($procName){
            // 2
            case 'whoIs':
                array_push($nP, '?');
            // 1
            case 'userProfil':
            case 'mc_coursesGroup':       // 1 String
            case 'mc_coursesGroupId':     // 1 int
            case 'mc_group':              // 1 String
            case 'mc_groupWithParent':    // 1 String
                array_push($nP, '?');
            // 0
            case 'mc_allGroups':          // No param
            case 'mc_allGroupWithParent': //No param
                try{
                    $call = 'call ' . $procName . '(' . join(',', $nP) . ')';
                    $sth = $this->_iPdo->prepare($call);
                    $sth->execute($paramArray);
                    return $sth->fetchAll();
                } catch (PDOException $e) {
                    $this->_pdoException = $e->getMessage();
                    $this->_iDebug->addMsg('PDOException : ' . $this->_pdoException);
                }
                break;

            default:
                $this->_pdoException = 'call impossible à ' . $procName;
                $this->_iDebug->addMsg($this->_pdoException);
        }
    }
}