<?php

require_once 'Debug.inc.php';

class Config
{
    private $iDebug = null;
    private $file = 'INC/config.ini.php';
    private $config = null;

    public function __construct($iDebug = null)
    {
        if ($iDebug instanceof Debug) {
            $this->iDebug = $iDebug;
        } else {
            $this->iDebug = new Debug(true);
        }
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    public function load($file = null)
    {


        if ( !is_null($file) ) $this->setFile($file);
        $this->config = parse_ini_file($this->file, true);
        return $this->config;
    }


    private function blocManager($blocName, $blocData, &$out)
    {
        $oKey = ['min', 'max', 'pas'];

        foreach ($oKey as $aKey) {
            $$aKey = ( isset($blocData[$aKey]) ? $blocData[$aKey] : null );
            unset($blocData[$aKey]);
        }

        if ( isset($blocData['choix']) ) {
            $choix = $blocData['choix'];
            unset($blocData['choix']);
        }

        foreach ($blocData as $dataName => $dataValue) {
            $inputType = 'text';
            $id = "{$blocName}_$dataName";
            $name = "{$blocName}[$dataName]";
            $out[] = "<label for='$id'>$dataName</label>";
            $options = '';

            switch ($dataName) {
                case 'comment':
                    $out[] = "<textarea cols='50' id='$id' name='$name' disabled required>";
                    $out[] = $dataValue;
                    $out[] = "</textarea><br>";
                    break;

                case 'type':
                    $out[] = "<span id='$id' class='checkList'>";
                    $types = explode('|', $dataValue);
                    foreach ($types as $type) {
                        if ( isset($choix) ) {
                            if ( in_array($type, $choix) ) {
                                $options = 'checked';
                            } else {
                                $options = '';
                            }
                        }

                        $out[] = "<input type='checkbox' 
                                         id='{$id}_$type' 
                                         name='{$blocName}[choix][]'
                                         value='$type'
                                         $options>";
                        $out[] = "<label for='{$id}_$type'>$type</label>";
                    }
                    break;

                case 'taille':
                    $inputType = 'number';

                    /**
                     * @var $min
                     * @var $max
                     * @var $pas
                     */
                    $options = ($min !== null ? " min=$min" : '')
                        . ($max !== null ? " max=$max" : '')
                        . ($pas !== null ? " step=$pas" : '');

                    if ($options) $options .= ' title="' . str_replace('step', 'pas', $options) . '"';

                default:
                    $out[] = "<input type=$inputType id='$id' name='$name' value='$dataValue' $options required><br>";
            }
        }
    }

    public function display()
    {
        unset($this->config['ERREUR']);
        unset($this->config['DB']);

        $out = ['<form id="modifConfig" class ="myForm" name="modifConfig" method="POST" action="formSubmit.html"> '];
        $out[] = '<fieldset class="masterFieldset"><legend>Formulaire de configuration</legend>';
        foreach ($this->config as $section => $values) {
            $out[] = "<fieldset><legend>$section</legend>";
            $this->blocManager($section, $values, $out);
            $out[] = '</fieldset>';
        }

        $out[] = '<input type="submit" name="submit[configForm]" value="envoi">'
            . '</fieldset></form>';
        $out[] = '<div id="cfgResult"> </div>';

        return implode("\n", $out);
    }

    public function save($filename = null)
    {
        if ( is_null($filename) ) $filename = $this->file;
        $this->load($filename);

        $oldConfig = $this->config;

        foreach ($oldConfig as $blocName => $blocData) {
            foreach ($blocData as $dataName => $dataValue) {
                if ($dataName == 'choix') $oldConfig[$blocName]['choix'] = [];
            }
        }

        $newConfig = array_replace_recursive($oldConfig, $_POST);
        unset($newConfig['senderForm']);
        unset($newConfig['request']);

        $out = [];
        foreach ($newConfig as $blocName => $blocData) {
            $out[] = "[$blocName]";
            foreach ($blocData as $dataName => $dataValue) {
                if ($dataName == 'choix') {
                    foreach ($dataValue as $choix) {
                        $out[] = "{$dataName}[] = \"$choix\"";
                    }
                } else {
                    $out[] = "$dataName = \"$dataValue\"";
                }
            }
        }

        $file = fopen($filename, 'w');
        if ($file) {
            fwrite($file, implode("\n", $out));
            fclose($file);
            $this->_config = $newConfig;
        }
    }
}
