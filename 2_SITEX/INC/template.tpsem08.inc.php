<section id="tpsem08">
    <fieldset id="tp08search">
        <legend>Groupe recherché</legend>
        <form id="formSearch" name="formSearch">
            <input type="text" id="textSearch" class="I" name="textSearch" placeholder="groupe recherché">
            <div>
                <span title="debut (Begin)">
                    <label for="B">&#8676;</label>
                    <input type="radio" name="posSearch" id="B" value="B">
                </span>

                <span title="milieu (In)">
                    <label for="I">&#8676;</label>
                    <input type="radio" name="posSearch" id="I" value="I" checked>
                    <label for="I">&#8677;</label>
                </span>

                <span title="fin (End)">
                    <input type="radio" name="posSearch" id="E" value="E">
                    <label for="E">&#8677;</label>
                </span>
            </div>

        </form>
    </fieldset>

    <fieldset id="tp08select">
        <legend>Suggestion</legend>
        <form name="formTP08" id="formTP08" action="formSubmit.html">
            <select name="select" id="select" size="10" title="choisissez le groupe à afficher">
                <option>pour</option>
                <option class="deja">test</option>
            </select>
        </form>
    </fieldset>

    <fieldset id="tp08result">
        <legend>Liste des cours</legend>

        <div>
            <p>Pas de groupe sélectionné</p>
        </div>
    </fieldset>
</section>

<style>
    #tpsem08 {
        display: flex;
        flex-wrap: wrap;
    }

    #tpsem08 legend {
        padding: .25em;
    }

    #tp08search, #tp08select, #tp08result {
        border: .1em solid rgba(128, 128, 128, .75);
        margin: .15em;
        padding: .25em;
        border-radius: .25em;
    }

    #formSearch {
        text-align: center;
    }

    #formSearch label {
        margin: 0;
        font-size: 1.5em;
    }

    #formSearch span {
        margin: .25em;
        padding: .25em;
        border: .05em outset darkgray;
        border-radius: 1em;
        background-color: rgba(255, 255, 255, .67);
    }

    #textSearch {
        height: 2em;
        width: 12em;
        font-size: 1.25em;
    }

    #textSearch.B {
        text-align: left;
    }

    #textSearch.I {
        text-align: center;
    }

    #textSearch.E {
        text-align: right;
    }

    #select {
        width: 10em;
        overflow: auto;
    }

    #select option.deja::before {
        content: '\21AA  ';
    }
</style>