<?php

require_once 'Debug.inc.php';

class anoRights
{
    protected $iDebug = null;
    protected $rqList = [ // la liste des requêtes autorisées pour les "anonymes"
        //'---ano---',
        'home',
        'formSubmit',
        'sendLogo',
        'logOn',
        'formLogin',
        'logOff'
    ];
    protected $edit;
    protected $activ;
    protected $reactiv;
    protected $mdpp;

    public function __construct($iDebug = null)
    {
        $this->iDebug = ($iDebug instanceof Debug) ? $iDebug : new Debug();
    }

    /**
     * @return array
     */
    public function getRqList()
    {
        return $this->rqList;
    }

    public function isAuthenticated()
    {
        return false;
    }

    public function isAdmin()
    {
        return false;
    }

    public function isSubadmin()
    {
        return false;
    }

    public function isEdit()
    {
        return $this->edit;
    }

    public function setEdit($edit = true)
    {
        $this->edit = $edit;
    }

    public function isActiv()
    {
        return $this->activ;
    }

    public function setActiv($activ = true)
    {
        $this->activ = $activ;
    }

    public function isReactiv()
    {
        return $this->reactiv;
    }

    public function setReactiv($reactiv = true)
    {
        $this->reactiv = $reactiv;
    }

    public function isMdpp()
    {
        return $this->mdpp;
    }

    public function setMdpp($mdpp = true)
    {
        $this->mdpp = $mdpp;
    }
}

class membRights extends anoRights {

    public function __construct($iDebug = null)
    {
        $rqList = [ // la liste des requêtes autorisées en plus pour les "membres"
            //'---member---',
            'tpSem08p02',
            'formTP08'
        ];
        parent::__construct($iDebug);
        $this->rqList = array_merge($this->rqList,$rqList);
    }

    public function isAuthenticated()
    {
        return true;
    }
}

class modoRights extends membRights {

    public function __construct($iDebug = null)
    {
        $rqList = [
            //'---modo---',
            'test',
            'testDb'
        ];
        parent::__construct($iDebug);
        $this->rqList = array_merge($this->rqList, $rqList);
    }
}

class sAdminRights extends modoRights {

    public function __construct($iDebug = null)
    {
        $rqList = [
            //'---sAdmin---',
            'affSessLog',
            'config'
        ];
        parent::__construct($iDebug);
        $this->rqList = array_merge($this->rqList, $rqList);
    }

    public function isSubadmin()
    {
        return true;
    }
}

class adminRights extends sAdminRights {

    public function __construct($iDebug = null)
    {
        $rqList = [
            //'---admin---',
            'affSessUser',
            'clrSessLog',
            'clrSessStart',
            'modifConfig'
        ];
        parent::__construct($iDebug);
        $this->rqList = array_merge($this->rqList, $rqList);
    }

    public function isAdmin()
    {
        return true;
    }
}