-- Requête 1
-- trouver nombre étudiants
select count(matricule) as nb_Etu
from test.etudiants; 

-- Requête 2
-- trouver nombre étudiants + 1TL1
select count(matricule) as nb_Etu
from test.etudiants
where classe like '1TL1';

-- Requête 3
-- Trouver le nombre d'étudiants par classe
select distinct Classe, count(Matricule) as nb_Etu
from test.etudiants
group by Classe;
						
-- Requête 4
-- Nombre d'étudiants par année-section
SELECT left(Classe, 2) as AnSec, count(*) as Nombre
FROM test.etudiants
GROUP BY AnSec;

-- Requête 5
-- Nombre d'étudiants par section
SELECT DISTINCT SUBSTRING(Classe,2,1) AS AnSec, COUNT(Matricule) AS Nombre 
FROM test.etudiants
GROUP BY AnSec;

-- Requête 6
-- Nombre de fois qu'un prénom revient
SELECT DISTINCT Prenom, count(Prenom) as Nombre
from test.etudiants
group by Prenom
order by Nombre asc;

-- Requête 7
-- Plus grand nombre de fois que revient un prénom
SELECT MAX(nb) AS MaxName
FROM (
	SELECT COUNT(Prenom) AS nb 
    FROM test.etudiants
    GROUP BY Prenom
    ) AS nbr;
    
-- Requête 8
-- Afficher les prénoms qui reviennent le plus de fois
-- A améliorer.
SELECT Prenom 
FROM (
	SELECT Prenom, count(Prenom) as nb 
    FROM test.etudiants
    GROUP BY Prenom
    HAVING nb = 25
    ) AS Prenom_Max;

-- Requête 9
-- Afficher nombre d'homonymes
SELECT DISTINCT Nom, Prenom, COUNT(*) AS nb
FROM test.etudiants AS t1 
WHERE EXISTS (
	SELECT Nom, Prenom
    FROM test.etudiants AS t2 
    WHERE t1.Matricule <> t2.Matricule 
    AND t1.Nom = t2.Nom 
    AND t1.Prenom = t2.Prenom
    );
    
-- Requête 6
-- Afficher les informations concernant les homonymes en détails (classe, matricule)
-- Fonctionne pour les 2, sauffit de changer la table sur laquelle on fait la requête
SELECT DISTINCT *
FROM test.etudiants_2 AS t1  
WHERE EXISTS (
	SELECT Nom, Prenom
    FROM test.etudiants_2 AS t2 
    WHERE t1.Matricule <> t2.Matricule 
    AND t1.Nom = t2.Nom 
    AND t1.Prenom = t2.Prenom
    );
    
    




