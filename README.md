# PHASE 01b - Gestion des requêts
## Points validés 
### Client side
**Date:** *07/03/2019*
- [x] 1.1 : Initialisation des déclencheurs de requêtes
- [x] 1.2 : Déclaration des premières requêtes 
- [x] 1.3 : Envoi de la requête précise 
- [x] 1.4 : Simulons les appels ajax de ces premières requêtes
- [x] 1.5 : Mise en place de l'appel ajax de base et réception des actions reçues
- [x] 1.6 : Mise en place de la traduction safe du JSON 
- [x] 1.7 : Mise en place du player des actions
- [x] 1.8 : Mise en place des premières actions
- [ ] 1.9 : Prise en compte des nouveaux déclencheurs de requêtes
    - Vérifier le point **1.9.1**, il est fonctionnel mais je suis pas sûr de la solution, y'a des petits hics.
- [x] 1.10 : Fonctionnalisation de la création des déclencheurs 
- [ ] 1.11 : Gestion des mailto
    - Le *mailto:* fonctionne bizarrement, à vérifier si possible.
- [ ] 1.12 : Actions suivantes
    - A vérifier ~ 
        - `1.12.1 : ` le $.each ne renvoie pas la partie "titre" demandée par le prof, la "data" est bien envoyée.
        - `1.12.3 : ` testSousMenu a finir 

### Server side
**Date:** *08/03/2019*
- [x] 2.1 : La classe `Request`
- [ ] 2.2 : Détection des requêtes
    - Pas d'affichage des requêtes  
- [ ] 2.3 : La classe `debug` 
- [ ] 2.4 : Validation de la requête
- [ ] 2.5 : Exécution de la requête
- [ ] 2.6 : La classe `action`
- [ ] 2.7 : Questions de type d'examen
- [ ] 2.8 : Conseils
- [ ] 2.9 : Test de toute la chaine client-serveur



## Functions (explications)
